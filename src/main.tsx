import React from "react";
import { render } from "react-dom";
import { Provider } from "react-redux";
import configureStore from "./app/store";
import TKCalendar from "./app/TKCalendar";
import "./main.css";

if (process.env.NODE_ENV !== "production") {
  document.addEventListener("DOMContentLoaded", function () {
    render(
      <Provider store={configureStore()}>
        <div className="tkc-dev-container">
          <TKCalendar />
        </div>
      </Provider>,
      document.body.appendChild(document.createElement("div"))
    );
  });
} else {
  const domContainer = document.querySelector("#calendar_container");
  render(
    <Provider store={configureStore()}>
      <TKCalendar />
    </Provider>,
    domContainer
  );
}
