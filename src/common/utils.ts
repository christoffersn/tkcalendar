import { EventInput } from "@fullcalendar/react";

export function getHashValues(hash: any) : EventInput[] {
    return Object.values(hash); // needs modern browser
}