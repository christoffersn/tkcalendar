import { cachekey } from './config';

interface storageItem {
    value: any
    expiry: number
}

export const CACHE_KEY = "TKC_" + cachekey;

export const clearCache = () => {
    var arr: string[] = []; // Array to hold the keys
    // Iterate over localStorage and insert the keys that meet the condition into arr
    for (var i = 0; i < localStorage.length; i++) {
        let key = localStorage.key(i);
        if(key !== null && key.substr(0,CACHE_KEY.length) == CACHE_KEY){
                arr.push(key);
        }
    }

    // Iterate over arr and remove the items by key
    for (var i = 0; i < arr.length; i++) {
        localStorage.removeItem(arr[i]);
    }
}

export const loadFromCache = (key: string) => {
    key = CACHE_KEY + key;
    try {
        const serializedState = localStorage.getItem(key);
        if (serializedState === null) {
            return undefined;
        }
        const item: storageItem = JSON.parse(serializedState);
        const now = new Date()
        if (item.expiry != -1 && now.getTime() > item.expiry) {
            // If the item is expired, delete the item from storage
            // and return null
            localStorage.removeItem(key)
            return undefined
        }

        return item.value
    } catch (err) {
        return undefined; //Handle any case where local storage is not allowed or similar
    }
};

export const saveToCache = (state: any, key: string, ttl = -1) => {
    key = CACHE_KEY + key;
    try {
        const now = new Date()
        const item: storageItem = {
            value: state,
            expiry: ttl === -1 ? -1 : now.getTime() + ttl
        }
        const serializedState = JSON.stringify(item);
        localStorage.setItem(key, serializedState)
    }
    catch (err) {
        // Ignore write errors.
    }
}