export const statusMap = {
    workShopDoneStatus : ["20", "30", "90"], //Currently not used
    deliverablePlannedStatus : ["10", "20"],
    deliverableCommentsStatus : ["32"],
    deliverableWrittenStatus : ["30", "31", "33"],
    deliverableReviewedStatus : ["40"],
    deliverableApprovedStatus : ["50"]
}

export const colors = {
    readycolor : "#5DADE2",
    notreadycolor : "#D4AC0D",
    donecolor : "green",
    delayedcolor : "red",
    defaultcolor : "#5DADE2",
    workshopcolor : "darkblue",
    vacationcolor : "#2c2c2c",
    iterationcolor : "#2c3e50",
    periodscolor: "#0033ff"
}

export const cachekey = "KMTSPK"; //Suggested value is just your toolkit name

export const baseurls = {
    api: "/cases/GTO775/KMTSPK/_api",
    workshops : "/cases/GTO775/KMTSPK/Lists/Workshops/DispForm.aspx?ID=",
    deliverables : "/cases/GTO775/KMTSPK/Deliverables/Forms/DispForm.aspx?ID=",
    allocationadjustments : "/cases/GTO775/KMTSPK/Lists/AllocationAdjustments/DispForm.aspx?ID=",
}

export const listTitles = {
    workshops : "Workshops",
    deliverables : "Slutprodukter",
    allocationadjustments : "Allokeringsændringer",
    iterations : "Iterationer",
    customCalendarPeriods: "KalenderPerioder",
}