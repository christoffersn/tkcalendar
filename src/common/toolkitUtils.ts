import { loadFromCache, saveToCache } from '../common/localStorage';
import { baseurls } from './config';

export function getItemsBaseUrl(listname: string): URL {
  return new URL(baseurls.api + `/lists/GetByTitle('${listname}')/items`, window.location.origin);
}

export function fetchToolkitItems<T>(url: URL, mapper: (e: any) => T, headers = getDefaultHeaders()): Promise<T[]> {
  return new Promise((resolve, reject) => {
    const path = url.toString();
    const json = loadFromCache(path);
    if(json)
      resolve(json.value.map(mapper))
    else{
      fetch(path, {
        headers: headers
      })
        .then(r =>
          r.json())
        .then(json => {
          saveToCache(json,path, 14400000); //4 hours expiry time
          let result = json.value.map(mapper);
          resolve(result);
        }
        )
        .catch(e => {
          console.log(e);
          reject(new Error('error'));
        })
    }
  })
}

export function getDefaultHeaders(): Headers {
  let requestHeaders: Headers = new Headers();
  requestHeaders.set("accept", "application/json;odata=nometadata");

  return requestHeaders;
}