import { createSlice } from "@reduxjs/toolkit"

const defaultFilters = { 
    VACATION: false,
    WORKSHOPS: true,
    DELIVERABLES: true, 
};

const teamsSlice = createSlice({
    name: 'filters',
    initialState: defaultFilters,
    reducers: {
        toggleVacation: (state) => { state.VACATION = !state.VACATION },
        toggleWorkshops: (state) => { state.WORKSHOPS = !state.WORKSHOPS },
        toggleDeliverables: (state) => { state.DELIVERABLES = !state.DELIVERABLES },
    }
})

// Extract the action creators object and the reducer
const { actions, reducer } = teamsSlice
// Extract and export each action creator by name
export const { toggleVacation, toggleWorkshops, toggleDeliverables } = actions
// Export the reducer, either as a default or named export
export default reducer
