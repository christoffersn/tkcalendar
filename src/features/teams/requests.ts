import { getItemsBaseUrl, fetchToolkitItems, getDefaultHeaders} from "../../common/toolkitUtils";
import { ResourceLane } from "../../app/types";

export function requestTeams(): Promise<ResourceLane[]>  {
  let url: URL = getItemsBaseUrl('Teams');
  url.searchParams.set("$filter",``);
  url.searchParams.set("$select", `Id,Title`);
  
  return fetchToolkitItems(url, mapTeamResponse);
}

function mapTeamResponse(response: any): ResourceLane{
  let event: ResourceLane = {
    id: response.Id,
    title: response.Title,
    team: response.Title
  }
  return event;
}