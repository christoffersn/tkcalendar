import { createSlice, PayloadAction } from "@reduxjs/toolkit"
import { requestTeams } from "./requests";
import {
    ResourcelaneArray,
    ResourceLane,
  } from "../../app/types";
import { TKCThunkResult } from "../../app/store"

const defaultResources = { "All": { id: "All", team: "All", title: "All - Iterations" }, "Vacation": { id: "Vacation", team: "Vacation", title: "Vacation"} };

const teamsSlice = createSlice({
    name: 'teams',
    initialState: defaultResources,
    reducers: {
        requestResourceLanesSuccess(state: ResourcelaneArray, action: { type: string, payload:  Array<ResourceLane> }) {
            action.payload.forEach(lane => {
                if (lane.id){
                    state[lane.id] = lane
                }
            })
        }
    }
})

export const {
    requestResourceLanesSuccess
} = teamsSlice.actions

export default teamsSlice.reducer

export const requestResourceLanes = (): TKCThunkResult<void> => 
async dispatch => {
    try {
        const lanes = await requestTeams()
        dispatch(requestResourceLanesSuccess(lanes))
    } catch (err) {
        console.log(err)
    }
}