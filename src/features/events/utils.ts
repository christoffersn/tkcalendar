import { EventInput } from "@fullcalendar/react";
import longestCommonSubstring from "../../common/longestcommonsubstring";

interface IterationMap { [key: string]: EventInput[] }


// Tricky function that works on a set of iterations and checks if any starts and ends on the same date suggesting duplications
// If any iterations have an identical period, it finds a name for the iteration based on the longest common substring.
//
// This is done because on some projects have created the same iteration multiple times - one for each team.
// This is no longer best practice, and therefore this mapping will rarely have any effect
export function mergeIterations(iterations: EventInput[] | undefined): EventInput[] {
  if(iterations === undefined)
    return [];
  //Build a map of start/enddates
  let map: IterationMap = {};
  let mergediterations: EventInput[] = [];

  iterations.forEach(iteration => {
    if (iteration.start && iteration.end) {
      const indexkey = iteration.start.toString() + iteration.end.toString()
      if (!map[indexkey]) {
        map[indexkey] = []
      }
      map[indexkey].push(iteration)
    }
  })

  //For each set, find the longest common substring and use that as title

  // To simplify the algorithm a bit: 
  // Find top 3 distinct common substrings between two first entries.
  // See if any of them exists in the others. If not, give up and add all iterations
  for (let entry in map) {
    let itrs = map[entry];

    //Simple case=There is just one
    if (itrs.length === 1) {
      mergediterations.push(itrs[0])
      continue;
    }

    if (itrs[0].title && itrs[1].title) {
      if (itrs.length === 2) {
        let title = ''

        title = longestCommonSubstring(itrs[0].title, itrs[1].title)

        //We require a minimum length of 4 to give some meaningful title
        if (title.length > 3)
          mergediterations.push({ ...itrs[0], title: trimTitle(title, "+*- _") })
        else
          itrs.forEach(x => mergediterations.push(x))
        continue;
      }

      //If more than two elements, try to find a common title
      //Look at the common titles betweent the first two strings, and checking if any exists in the other strings
      if (itrs.length > 2) {
        const title1 = longestCommonSubstring(itrs[0].title, itrs[1].title)
        const title2 = longestCommonSubstring(itrs[0].title.replace(title1, ''), itrs[1].title.replace(title1, ''))
        const title3 = longestCommonSubstring(itrs[0].title.replace(title1, '').replace(title2, ''), itrs[1].title.replace(title1, '').replace(title2, ''))

        //We require a minimum length of 4 to give some meaningful title
        if (title1.length > 3 && itrs.every(x => x.title?.includes(title1))) { mergediterations.push({ ...itrs[0], title: trimTitle(title1, "+*- _") }); continue; }
        if (title2.length > 3 && itrs.every(x => x.title?.includes(title2))) { mergediterations.push({ ...itrs[0], title: trimTitle(title2, "+*- _") }); continue; }
        if (title3.length > 3 && itrs.every(x => x.title?.includes(title3))) { mergediterations.push({ ...itrs[0], title: trimTitle(title3, "+*- _") }); continue; }

        //If none matches, we just add them all indivually
        itrs.forEach(x => mergediterations.push(x))
      }
    }
  }
  return mergediterations;
}

//Trim the title for special characters
var trimTitle = (function () {
  "use strict";

  function escapeRegex(string: string) {
      return string.replace(/[\[\](){}?*+\^$\\.|\-]/g, "\\$&");
  }

  return function trim(str: string, characters: any) {
      let flags = "g";

      characters = escapeRegex(characters);

      return str.replace(new RegExp("^[" + characters + "]+|[" + characters + "]+$", flags), '');
  };
}());