import { createSlice } from "@reduxjs/toolkit"
import { EventInput } from "@fullcalendar/react"
import { EventArray } from "../../app/types"
import { TKCThunkResult } from "../../app/store"
import {
    requestWorkshopsInRange,
    requestEventCreate,
    requestEventUpdate,
    requestEventDelete,
    requestDeliverablesInRange,
    requestIterationsInRange,
    requestAllocationAdjustmentsInRange,
    requestCalendarPeriodsInRange
} from "./requests";
import { mergeIterations } from "./utils"
import { clearCache } from "../../common/localStorage"

const eventSlice = createSlice({
    name: 'events',
    initialState: {},
    reducers: {
        requestEventsSuccess(state: EventArray, action: { type: string, payload: { items: EventInput[] } }) {
            for (let e in state) {
                delete state[e];
            }
            action.payload.items.forEach(event => {
                if (event.id)
                    state[event.id] = event
            })
        },
        createEventSuccess(state: EventArray, action: { type: string, payload: EventInput }) {
            if (action.payload.id)
                state[action.payload.id] = action.payload
        },
        updateEventSuccess(state: EventArray, action: { type: string, payload: EventInput }) {
            if (action.payload.id)
                state[action.payload.id] = action.payload
        },
        deleteEventSuccess(state: EventArray, action: { type: string, payload: string }) {
            delete state[action.payload]
        }
    }
})

export const {
    requestEventsSuccess,
    createEventSuccess,
    updateEventSuccess,
    deleteEventSuccess } = eventSlice.actions

export default eventSlice.reducer


//Action creators
export const requestEvents = (
    start: Date,
    end: Date,
    shouldClearCache: boolean = false
): TKCThunkResult<void> => async dispatch => {
    if (shouldClearCache) {
        clearCache();
    }
    try {
        const p1 = requestWorkshopsInRange(start, end);
        const p2 = requestDeliverablesInRange(start, end)
        const p3 = requestAllocationAdjustmentsInRange(start, end)
        const p4 = requestCalendarPeriodsInRange(start,end)
        const p5 = requestIterationsInRange(start, end) //Has to be added last to the array to figure out which promise to run the mergeIterations function on
        const promises = [p1,p2,p3,p4,p5];

        Promise.all(promises).then((events) => {
           events.push(mergeIterations(events.pop())) //Iterations HAS to be last in the promise array for this to work //TODO find a better solution
           let flat: EventInput[] = [];
           flat = flat.concat.apply([], events)

           dispatch(requestEventsSuccess({ items: flat }))
        })
        
    } catch (err) {
        console.log(err)
    }
}

//Dummy - Not used currently - Could be extended to make the calendar interactive
export const createEvent = (
    plainEventObject: EventInput
): TKCThunkResult<void> => async dispatch => {
    try {
        const newEventId = await requestEventCreate(plainEventObject)
        dispatch(createEventSuccess({
            id: newEventId,
            ...plainEventObject
        }))
    } catch (err) {
        console.log(err)
    }
}

//Dummy - Not used currently - Could be extended to make the calendar interactive
export const updateEvent = (
    plainEventObject: EventInput
): TKCThunkResult<void> => async dispatch => {
    try {
        await requestEventUpdate(plainEventObject)
        dispatch(updateEventSuccess(plainEventObject))
    } catch (err) {
        console.log(err)
    }
}

//Dummy - Not used currently - Could be extended to make the calendar interactive
export const deleteEvent = (
    eventId: string
): TKCThunkResult<void> => async dispatch => {
    try {
        await requestEventDelete(eventId)
        dispatch(deleteEventSuccess(eventId))
    } catch (err) {
        console.log(err)
    }
}
