import { EventInput } from "@fullcalendar/react";
import { getItemsBaseUrl, fetchToolkitItems } from "../../common/toolkitUtils";

import { statusMap, colors, baseurls, listTitles } from "../../common/config";

export function requestWorkshopsInRange(start: Date, end: Date): Promise<EventInput[]> {
  let url: URL = getItemsBaseUrl(listTitles.workshops);
  url.searchParams.set("$filter", `Dato ge '${start.toUTCString()}' and Dato le '${end.toUTCString()}'`);
  url.searchParams.set("$select", `Id,Dato,Title,Status,TeamId`);

  return fetchToolkitItems(url, mapWorkshopResponse);
}


export function requestDeliverablesInRange(start: Date, end: Date): Promise<EventInput[]> {
  let url: URL = getItemsBaseUrl(listTitles.deliverables);
  url.searchParams.set("$filter", `Deadline le '${end.toUTCString()}' and Deadline ge '${start.toUTCString()}'` +
                                  `or DeadlineCompletion le '${end.toUTCString()}' and DeadlineCompletion ge '${start.toUTCString()}'`);
  url.searchParams.set("$select", `Id,Deadline,DeadlineCompletion,Title,DocumentStatus,TeamId`);

  return fetchToolkitItems(url, mapDeliverablesResponse);
}

export function requestAllocationAdjustmentsInRange(start: Date, end: Date): Promise<EventInput[]> {
  let url: URL = getItemsBaseUrl(listTitles.allocationadjustments);
  url.searchParams.set("$filter", `DateFrom le '${end.toUTCString()}' and DateTo ge '${start.toUTCString()}'`);
  url.searchParams.set("$select", `Id,DateFrom,Title,DateTo,TeamId`);

  return fetchToolkitItems(url, mapAllocationAdjustmentsResponse);
}


export function requestIterationsInRange(start: Date, end: Date): Promise<EventInput[]> {
  let url: URL = getItemsBaseUrl(listTitles.iterations);
  url.searchParams.set("$filter", `StartDate le '${end.toUTCString()}' and EndDate ge '${start.toUTCString()}'`);
  url.searchParams.set("$select", `Id,Title,StartDate,EndDate`);

  return fetchToolkitItems(url, mapIterationsResponse);
}


export function requestCalendarPeriodsInRange(start: Date, end: Date): Promise<EventInput[]> {
  let url: URL = getItemsBaseUrl(listTitles.customCalendarPeriods);
  url.searchParams.set("$filter", `EventDate le '${end.toUTCString()}' and EndDate ge '${start.toUTCString()}'`);
  url.searchParams.set("$select", `Id,Title,EventDate,EndDate`);

  return fetchToolkitItems(url, mapCalendarPeriodResponse);
}

function mapWorkshopResponse(response: { [key: string]: any }): EventInput {
  let event: EventInput = {
    id: "workshop-" + response.Id,
    title: response.Title,
    start: response.Dato,
    resourceId: response.TeamId.toString(),
    tklist: "Workshops",
    color: colors.workshopcolor,
    url: baseurls.workshops + response.Id
  }
  return event;
}


function mapDeliverablesResponse(response: { [key: string]: any }): EventInput {
  if (!response.DeadlineCompletion) {
    response.DeadlineCompletion = response.Deadline
  }
  let enddate = new Date(response.Deadline);
  enddate.setDate(enddate.getDate() + 1)
  if(!response.TeamId){
    response.TeamId = "All";
  }
  let event: EventInput = {
    id: "deliverable-" + response.Id,
    title: response.Title,
    start: response.DeadlineCompletion,
    end: enddate,
    resourceId: response.TeamId,
    tklist: "Deliverables",
    color: calculateDeliverableColor(response.DocumentStatus, new Date(response.DeadlineCompletion), enddate),
    url: baseurls.deliverables + response.Id
  }
  return event;
}

function calculateDeliverableColor(status: String, start: Date, end: Date): string {
  let statusnumber = status.substr(0, 2);

  if (statusMap.deliverablePlannedStatus.includes(statusnumber)) {
    if (start < new Date()) {
      return colors.delayedcolor;
    }
    return colors.notreadycolor;
  }
  if (statusMap.deliverableWrittenStatus.includes(statusnumber)) {
    if (end < new Date()) {
      return colors.delayedcolor;
    }
    return colors.readycolor;
  }
  if (statusMap.deliverableCommentsStatus.includes(statusnumber)) {
    return colors.notreadycolor;
  }
  if (statusMap.deliverableReviewedStatus.includes(statusnumber)) {
    return colors.donecolor;
  }
  if (statusMap.deliverableApprovedStatus.includes(statusnumber)) {
    return colors.donecolor;
  }

  return colors.defaultcolor;
}

function mapAllocationAdjustmentsResponse(response: { [key: string]: any }): EventInput {
  let enddate = new Date(response.DateTo);
  enddate.setDate(enddate.getDate() + 1)
  let event: EventInput = {
    id: "adjustment-" + response.Id,
    title: response.Title,
    start: response.DateFrom,
    end: enddate.toISOString(),
    resourceId: "Vacation",
    color: colors.vacationcolor,
    tklist: "AllocationAdjustments",
    url: baseurls.allocationadjustments + response.Id
  }
  return event;
}

function mapCalendarPeriodResponse(response: { [key: string]: any }): EventInput {
  let enddate = new Date(response.EndDate);
  enddate.setDate(enddate.getDate() + 1)
  let event: EventInput = {
    id: "calendarperiod-" + response.Id,
    title: response.Title,
    start: response.EventDate,
    end: enddate.toISOString(),
    resourceId: 'All',
    color: colors.periodscolor,
    tklist: "KalenderPerioder"
  }
  return event;
}

function mapIterationsResponse(response: { [key: string]: any }): EventInput {
  let enddate = new Date(response.EndDate);
  enddate.setDate(enddate.getDate() + 1)
  let event: EventInput = {
    id: "iteration-" + response.Id,
    title: response.Title,
    start: response.StartDate,
    end: enddate.toISOString(),
    resourceId: 'All',
    color: colors.iterationcolor,
    tklist: "Iterations"
  }
  return event;
}



/*************** OLD STUBS - They can be used to mocking editing events ********************/

export function requestEventCreate(plainEventObject: any): Promise<string> {
  console.log('[STUB] requesting event create:', plainEventObject)

  return new Promise((resolve, reject) => {
    setTimeout(() => {
      //let objWithId = {...plainEventObject, id: newEventId}
      //eventDb.push(objWithId)
      resolve("1")
    }, 0)
  })
}

export function requestEventUpdate(plainEventObject: EventInput): Promise<EventInput[]> {
  console.log('[STUB] requesting event update:', plainEventObject)

  return new Promise((resolve, reject) => {
    setTimeout(() => {
      //eventDb = excludeById(eventDb, plainEventObject.id)
      //eventDb.push(plainEventObject)
      //resolve(eventDb)
    }, 0)
  })
}

export function requestEventDelete(eventId: string): Promise<EventInput[]> {
  console.log('[STUB] requesting event delete, id:', eventId)

  return new Promise((resolve, reject) => {
    setTimeout(() => { // simulate network delay
      //eventDb = excludeById(eventDb, eventId)
      // resolve(eventDb)
    }, 0)
  })
}
