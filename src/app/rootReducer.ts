import {combineReducers} from "redux"
import eventsReducer  from "../features/events/eventSlice";
import teamsReducer from "../features/teams/teamsslice";
import filterReducer from "../features/filters/filterSlice";

const rootReducer = combineReducers({
    events: eventsReducer,
    resourcelanes: teamsReducer,
    filters : filterReducer
  });

export default rootReducer

export type RootState = ReturnType<typeof rootReducer>