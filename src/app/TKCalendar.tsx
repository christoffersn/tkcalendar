import React from "react";
import { connect, ConnectedProps } from "react-redux";
import { createSelector } from "@reduxjs/toolkit";
import FullCalendar, {
  DateSelectArg,
  EventClickArg,
  EventContentArg,
  formatDate,
  EventInput,
  EventAddArg,
  EventChangeArg,
  EventRemoveArg,
  DatesSetArg,
} from "@fullcalendar/react";
import resourceTimelinePlugin from "@fullcalendar/resource-timeline";
import { ResourceLane, ResourcelaneArray, EventArray } from "./types";
import {
  requestEvents,
  createEvent,
  updateEvent,
  deleteEvent,
} from "../features/events/eventSlice";
import { requestResourceLanes } from "../features/teams/teamsslice"
import { toggleDeliverables, toggleVacation, toggleWorkshops } from "../features/filters/filterSlice"
import { getHashValues } from "../common/utils";
import { RootState } from "./rootReducer";
import ReactTooltip from "react-tooltip";
import { colors } from "../common/config";

interface TKCProps extends PropsFromRedux { }
interface TKCState { start: Date, end: Date }

class TKCalendar extends React.Component<TKCProps, TKCState> {
  constructor(props: TKCProps) {
    super(props);
    try {
      this.props.requestResourceLanes();
    } catch {
      reportNetworkError;
    }
  }

  render() {
    return (
      <div className="TKCalendar-app">
        {process.env.NODE_ENV !== 'production' ? this.renderSidebar() : ''}
        <div className="TKCalendar-content">
          {this.renderHeader()}
          <div className="TKCalendar-app-main">
            <FullCalendar
              plugins={[resourceTimelinePlugin]}
              customButtons={{
                refresh: {
                  text: "refresh",
                  click: this.handleRefresh
                },
                toggleVacation: {
                  text: "vacation",
                  click: this.props.toggleVacation
                },
                toggleDeliverables: {
                  text: "deliverables",
                  click: this.props.toggleDeliverables
                },
                toggleWorkshops: {
                  text: "workshops",
                  click: this.props.toggleWorkshops
                }
              }}
              headerToolbar={{
                left: "prev,next refresh",
                center: "title",
                right:
                  "toggleVacation,toggleDeliverables,toggleWorkshops resourceTimelineMonth,fourweeksrunning",
              }}
              initialView="fourweeksrunning"
              editable={true}
              selectable={true}
              selectMirror={true}
              dayMaxEvents={true}
              weekends={true}
              datesSet={this.handleDates}
              events={this.props.events}
              eventContent={renderEventContent} // custom render function
              resourceOrder={"title"}
              /*select={this.handleDateSelect}
              eventClick={this.handleEventClick}
              eventAdd={this.handleEventAdd}
              eventChange={this.handleEventChange} // called for drag-n-drop/resize
              eventRemove={this.handleEventRemove}*/
              schedulerLicenseKey={"GPL-My-Project-Is-Open-Source"}
              height={"auto"}
              views={{
                fourweeksrunning: {
                  type: "resourceTimeline",
                  duration: { weeks: 4 },
                  buttonText: "4 weeks running",
                },
              }}
              //resourceGroupField="team"
              resourceAreaWidth={150}
              slotLabelFormat={[{ day: "numeric" }, { weekday: "short" }]}
              resources={this.props.resourcelanes}
            />
          </div>
        </div>
      </div>
    );
  }

  renderHeader() {
    return (
      <div className="TKCalendar-app-header">
        <div className="TKCalendar-app-colors">
          <span style={{ backgroundColor: colors.notreadycolor }}>Awaiting NC</span>
          <span style={{ backgroundColor: colors.readycolor }}>Ready for review</span>
          <span style={{ backgroundColor: colors.donecolor }}>Reviewed</span>
          <span style={{ backgroundColor: colors.delayedcolor }}>Deadline not completed</span>
          <span style={{ backgroundColor: colors.workshopcolor }}>Workshops</span>
        </div>
      </div>
    );
  }

  renderSidebar() {
    return (
      <div className="TKCalendar-app-sidebar">
        <div className="TKCalendar-app-sidebar-section">
          <h2>Instructions</h2>
          <ul>
            <li>Select dates and you will be prompted to create a new event</li>
            <li>Drag, drop, and resize events if you wish</li>
            <li>Click an event to delete it</li>
          </ul>
        </div>
        <div className="TKCalendar-app-sidebar-section">
          <label>
            {/*  <input
              type="checkbox"
              checked={this.props.weekendsVisible}
              onChange={this.props.toggleWeekends()}
            ></input>*/}
            toggle weekends
          </label>
        </div>
        <div className="TKCalendar-app-sidebar-section">
          <h2>All Events ({this.props.events.length})</h2>
          <ul>{this.props.events.map(renderSidebarEvent)}</ul>
        </div>
      </div>
    );
  }

  handleDates = (rangeInfo: DatesSetArg) => {
    try {
      this.setState({
        start: rangeInfo.start,
        end: rangeInfo.end
      })
      this.props.requestEvents(rangeInfo.start, rangeInfo.end);
    } catch {
      reportNetworkError;
    }
  };

  handleRefresh = () => {
    this.props.requestEvents(this.state.start, this.state.end, true);
  }

  handleEventAdd = (addInfo: EventAddArg) => {
    let eventobj = addInfo.event.toPlainObject();
    if (addInfo.event.getResources().length > 0)
      eventobj.resourceId = addInfo.event.getResources()[0].id;
    try {
      this.props.createEvent(eventobj);
    } catch {
      reportNetworkError();
      addInfo.revert();
    }
  };

  handleEventChange = (changeInfo: EventChangeArg) => {
    let eventobj = changeInfo.event.toPlainObject();
    if (changeInfo.event.getResources().length > 0)
      eventobj.resourceId = changeInfo.event.getResources()[0].id;

    try {
      this.props.updateEvent(eventobj);
    } catch {
      reportNetworkError();
      changeInfo.revert();
    }
  };

  handleEventRemove = (removeInfo: EventRemoveArg) => {
    try {
      this.props.deleteEvent(removeInfo.event.id);
    } catch {
      reportNetworkError();
      removeInfo.revert();
    }
  };

  handleDateSelect = (selectInfo: DateSelectArg) => {
    let calendarApi = selectInfo.view.calendar;

    let title = prompt("Please enter a new title for your event");

    calendarApi.unselect(); // clear date selection

    if (title) {
      calendarApi.addEvent(
        {
          title,
          start: selectInfo.startStr,
          end: selectInfo.endStr,
          allDay: selectInfo.allDay,
          resourceId: selectInfo.resource?.id,
        },
        true
      );
    }
  };

  handleEventClick = (clickInfo: EventClickArg) => {
    if (
      confirm(
        `Are you sure you want to delete the event '${clickInfo.event.title}'`
      )
    ) {
      clickInfo.event.remove();
    }
  };
}

function renderEventContent(eventContent: EventContentArg) {
  return (
    <div data-for="main" data-tip={eventContent.event.title} className="fc-event-main-frame">
      <div className="fc-event-title-container">
        <div className="fc-event-title fc-sticky">
          {eventContent.event.title}
        </div>
      </div>

      <ReactTooltip id="main" place="bottom" type="dark"></ReactTooltip>
    </div>

  );
}

function renderSidebarEvent(event: EventInput) {
  return (
    <li key={event.id}>
      <b>
        {formatDate(event.start!, {
          year: "numeric",
          month: "short",
          day: "numeric",
        })}
      </b>
      <i>{event.title}</i>
      <span> res_id: {event.resourceId}</span>
    </li>
  );
}

function filterResourceLanes(
  events: EventArray,
  lanes: ResourcelaneArray,
  filters: any
): ResourceLane[] {
  let filteredlanes: ResourcelaneArray = {};
  let eventarray = applyEventFilters(events, filters);
  for (let lane in lanes) {
    if (eventarray.some((x) => x.resourceId == lanes[lane].id))
      filteredlanes[lane] = lanes[lane];
  }
  return Object.values(filteredlanes);
}

function applyEventFilters(events: EventArray, filters: any): EventInput[] {
  let eventarray = getHashValues(events);
  if (!filters.VACATION) {
    eventarray = eventarray.filter(e => e.tklist != "AllocationAdjustments")
  }
  if (!filters.WORKSHOPS) {
    eventarray = eventarray.filter(e => e.tklist != "Workshops")
  }
  if (!filters.DELIVERABLES) {
    eventarray = eventarray.filter(e => e.tklist != "Deliverables")
  }
  return eventarray;
}

function reportNetworkError() {
  alert("This action could not be completed");
}

const mapStateToProps = () => {
  const getEventArray = createSelector(
    (state: RootState) => state.events,
    (state: RootState) => state.filters,
    applyEventFilters
  );

  const getResourceArray = createSelector(
    (state: RootState) => state.events,
    (state: RootState) => state.resourcelanes,
    (state: RootState) => state.filters,
    filterResourceLanes
  );

  return (state: RootState) => {
    return {
      events: getEventArray(state),
      resourcelanes: getResourceArray(state),
      filters: state.filters
    };
  };
};


/* Typing needed for Redux/Thunk */

const mapState = (state: RootState) => mapStateToProps();

const mapDispatch = {
  requestEvents,
  createEvent,
  updateEvent,
  deleteEvent,
  requestResourceLanes,
  toggleDeliverables,
  toggleVacation,
  toggleWorkshops
};

const connector = connect(mapState, mapDispatch);
type PropsFromRedux = ConnectedProps<typeof connector>;

export default connector(TKCalendar);
//export default connect(mapStateToProps, mapDispatch)(TKCalendar) // Expanded version of the line above
