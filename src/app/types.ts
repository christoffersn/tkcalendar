import  { EventInput } from "@fullcalendar/react";

/** Application model */
export interface ResourceLane {
    id: string
    title: string
    team: string
}

export type ResourcelaneArray = {[key: string]: ResourceLane}
export type EventArray = {[key: string]: EventInput}
