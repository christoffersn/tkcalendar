import { configureStore, Action } from '@reduxjs/toolkit'
import thunkMiddleware, { ThunkAction, ThunkDispatch } from "redux-thunk";
import rootReducer, { RootState } from "./rootReducer";

export default function getStore() {
  const middlewares = process.env.NODE_ENV !== 'production' ?
    [require('redux-immutable-state-invariant').default(), thunkMiddleware] :
    [thunkMiddleware];

  const store = configureStore({
    reducer: rootReducer,
    middleware: middlewares
  });

  return store;
}

/** Thunk generic types */
export type TKCThunkResult<R> = ThunkAction<R, RootState, null, Action<String>>

export type TKCThunkDispatch = ThunkDispatch<RootState, null, Action<String>>