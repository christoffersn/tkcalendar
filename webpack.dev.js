const { merge } = require('webpack-merge');
const common = require('./webpack.common.js');

module.exports = merge(common, {
mode: 'development',
  devServer: {
    host: 'localhost',
    port: 8080,
    open: true,
    historyApiFallback: true,
    proxy: {
      '/cases/GTO775/KMTSPK': {
        target: 'http://localhost:3065',
        secure: false
      }
    }
  }
});