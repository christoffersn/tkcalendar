# TKCalendar

## Background
This javascript project creates a Calendar that can be shown on Netcompany sharepoint toolkits showing events from multiple lists. 
Example can be found on the KMTSPK toolkit here: https://goto.netcompany.com/cases/GTO775/KMTSPK/SitePages/CalendarOverview.aspx
Special thanks to Søren Frejstrup Maibing for the original calendar on the TopDanmark project.

## Installation
To install the plugin, download the source code and follow the 5 steps below. Prerequisites is to have node.js and npm installed on your computer.
#### 1 Custom toolkit list
The calendar mostly uses standard toolkit lists, but expects one custom list for adding events that doesn't fit in anywhere else e.g. a period where the customer is unavailable or a major project milestone.
The list requires the following fields: Id,Title,EventDate,EndDate. If you create a new list of type "Calendar" those will be added as standard.
Use the name of the list in the configuration file in Step 2.
#### 2 Update configuration file
The configuration file can be found in ``src/common/config.ts``
The configuration keys are hopefully self-explanatory, as a minimum you should edit the cachekey and the baseurls to fit your toolkit. If you are on an english toolkit the list titles also needs to be updated.
#### 3 - Build and upload javascript files
If you havn't already, install the development environment by running
```bash 
npm install
```
Then build the javascript files:
```bash 
npm run build
```
This will create a new folder ``/dist`` that contains the minified calendar javascript code. Upload it somewhere to your toolkit, and remember the path for step 3.3.
The recommened path is ../SiteAssets/CustomJS/ which is used in the example below, but you can place it inside your document library also.

**Note:**
Building and uploading is automated by the following command, if you are doing a lot of reconfiguration you can reconfigure the **publish** task in ``package.json`` with your own toolkit path. Afterwards just run:
```bash 
npm run publish
```
#### 4 - Create calendar page
If you want to include the calendar on an existing page, e.g. the front page, you can skip this step.
Otherwise create a new page on the toolkit to contain the calendar.
You can do that from the toolkits list of SitePages, e.g. https://goto.netcompany.com/cases/GTO775/KMTSPK/SitePages/Forms/AllPages.aspx
Replace the toolkit root URL with your own toolkit.
#### 5 - Insert javascript
Then edit the page in Sharepoint and insert a ScriptEditor with the following snippet as content:
```bash
<div id="calendar_container"></div>
<script type="text/javascript" src="../SiteAssets/CustomJS/TKCalendar.js?ver=1"></script>
```
Remember to update the path if you placed the TKCalendar.js file somewhere else
## Developer notes
Project is written with React Redux and Typescript and is originaly based on the example project from FullCalendar: [FullCalendar's React Connector](https://fullcalendar.io/docs/react) & https://github.com/fullcalendar/fullcalendar-example-projects/tree/master/react-typescript 

The visual elements are defined in `app/TKCalendar.tsx` and most of the toolkit specific logic is in the `features/` folder. The rest is mainly config and "glue".

## Setup
```bash
npm install
```
The proxy should then be configured with authentication set to *User credentials (NTLM)* and with your toolkit URL as the baseURL. To start configuration, run the proxy:
```bash
npm run proxy
```
## Build Commands
```bash
npm run startServers # Opens a web browser with a Sharepoint proxy in the background. 
```
# Other commands:
```
npm run proxy # Run this if you havn't configured the Sharepoint proxy before 
npm run build # builds files into dist/ directory
npm run watch # same as build, but watches for changes
npm run publish # copies the output .js file to the toolkit
npm run clean # start a fresh build
```

